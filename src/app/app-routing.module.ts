import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddtripsComponent } from './components/addtrips/addtrips.component'
import { MytripsdisplayComponent } from './components/mytripsdisplay/mytripsdisplay.component'
import { HomeComponent } from './components/home/home.component'
import { LoginComponent } from './security/login/login.component'
import { RegisterComponent } from './security/register/register.component'
import { ResetpasswordComponent } from './security/resetpassword/resetpassword.component'
import { AuthGuard } from './services/auth.guard';
import { TutorialComponent } from './components/tutorial/tutorial.component'
import { AlltripsComponent } from './components/alltrips/alltrips.component';
import { FaqComponent } from './components/faq/faq.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { ChatAppComponent } from './chat/chat-app/chat-app.component';
const routes: Routes = [
  { path: 'addtrips', component: AddtripsComponent, canActivate: [AuthGuard] },
  { path: 'mytrips', component: MytripsdisplayComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'resetpw', component: ResetpasswordComponent},
  { path: 'alltrips', component: AlltripsComponent, },
  { path: 'tutorial', component: TutorialComponent, canActivate: [AuthGuard]},
  { path: 'faq', component: FaqComponent,},
  { path: 'calculator', component: CalculatorComponent,},
  { path: 'chatapp', component: ChatAppComponent, }, 
  { path: '', component: HomeComponent},

  { path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes),
  ],
  
})
export class AppRoutingModule {

  
 }


import { Component } from '@angular/core';
import { ngxZendeskWebwidgetService } from 'ngx-zendesk-webwidget';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'drivex';
  constructor(private _ngxZendeskWebwidgetService: ngxZendeskWebwidgetService) {
    _ngxZendeskWebwidgetService.identify({
      name: 'Name',
      email: 'Email'
     })
     _ngxZendeskWebwidgetService.show()
   }
  

  }


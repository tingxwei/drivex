import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormControl, FormGroup, Validators, NgForm, Form, ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from '../../services/auth.service';
import * as firebase from 'firebase/app';

import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  passReset: boolean = false;
  form: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private dataService: DataService,) { 
    this.form = this.fb.group({
      email: ['', Validators.required],
    })
  }

  
  ngOnInit() {
  }

  reset() {
    const val = this.form.value;
    this.authService.resetPassword(val.email)
  .then(() => this.passReset = true)
  }
}

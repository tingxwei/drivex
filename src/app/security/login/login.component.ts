import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormArray, FormControl, FormGroup, Validators, NgForm, Form, ReactiveFormsModule} from "@angular/forms";
import { Router, ActivatedRoute} from "@angular/router";
import { AuthService } from '../../services/auth.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form:FormGroup;
  constructor(private fb:FormBuilder,
    private authService: AuthService,
    private router:Router,private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['',Validators.required],
      password: ['',Validators.required]
  });  
    
  }

 /* signInWithGoogle() {
   
    console.log("attempting social google login...");
    this.authService.googleLogin().then((result)=>{
      console.log(result);
      //this.router.navigate([' ']);
    }).catch((error)=> console.log(error));
    console.log("social google login failed");
    
  }*/


  login() {
    this.spinnerService.show();
    const formValue = this.form.value;
    this.authService.login(formValue.email, formValue.password)
          .subscribe(
              (result) => {
                console.log(result);
                this.authService.setTokenIdToLocalstorage();
                
                setTimeout(function() {
                    this.spinnerService.hide();
                    this.router.navigate(['alltrips']);
                  }.bind(this), 800);
                
              }
          )
  }

}

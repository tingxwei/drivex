import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormArray, FormControl, FormGroup, Validators, NgForm, Form, ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from '../../services/auth.service';
import * as firebase from 'firebase/app';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

isSignedUp= false  
  setPaid: Observable<any>;
  status = 1
  form: FormGroup;
  itemRef: any;
  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private dataService: DataService, private modalService: NgbModal ) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirm: ['', Validators.required]
    })
  }

  ngOnInit() {
  }


  isPasswordMatch() {
    const val = this.form.value;
    return val && val.password && val.password == val.confirm;

  }

  signUp() {
    const val = this.form.value;

    this.authService.signUp(val.email, val.password)
      .subscribe(
      () => {
        this.isSignedUp = true;
        setTimeout(function() {
          this.router.navigate(['tutorial']);
          this.addCount()
        }.bind(this), 1200);
        
      },
      err => alert(err)
      );
      
  }


  goHome() {
    this.router.navigate(['login']);
  }

addCount (){
// this.dataService.addCount.subscribe(()=>
// console.log('user added to count'))
}
}

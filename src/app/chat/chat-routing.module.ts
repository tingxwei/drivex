import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChatService } from '../services/chat.service';
import { PusherService } from '../services/pusher.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatAppComponent} from './chat-app/chat-app.component'
import { ChatComponent} from './chat/chat.component'
import { MyDetailsComponent} from './my-details/my-details.component'

const chatRoutes: Routes = [
  { path: 'chat',  component: ChatComponent },

];
@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(chatRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class ChatRoutingModule { }

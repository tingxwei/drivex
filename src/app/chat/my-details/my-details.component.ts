import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChatService } from '../../services/chat.service';
import { Subscription } from 'rxjs'
import { AuthService } from '../../services/auth.service'
import { AuthInfo } from "../../services/auth-info";

@Component({
  selector: 'app-my-details',
  templateUrl: './my-details.component.html',
  styles: []
})
export class MyDetailsComponent implements OnInit {
  detailsForm: FormGroup;

  loader;
  result: any
  user = '';
  auth$: Subscription;
  constructor(private _fb: FormBuilder, private _chatService: ChatService, private ASvc: AuthService, ) { }

  ngOnInit() {

    this.auth$ = this.ASvc.authInfo$.subscribe(res => {
      this.result = res;
      this.user = this.result.email
      console.log(this.user);
      this._createForm();
    })
  }

  private _createForm() {
    this.detailsForm = this._fb.group({
      displayName: [this.user, Validators.required],
      email: [this.user, [Validators.required, Validators.email]]
    });
  }

  onSubmit() {
    const param = this.detailsForm.value;
    this._chatService.join(param)
      .subscribe((resp) => {
        this.loader = false;
      },
      (error) => {
        console.error(error);
        this.loader = false;
      });
  }

}

import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { faCommentsDollar } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-chat-app',
  templateUrl: './chat-app.component.html',
  styleUrls: ['./chat-app.component.css']
})
export class ChatAppComponent implements OnInit {
  faCommentsDollar=faCommentsDollar
  constructor(public chatService: ChatService, ) { }

  ngOnInit() {
    
  }

}

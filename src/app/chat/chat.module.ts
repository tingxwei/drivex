import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatRoutingModule } from './/chat-routing.module';
import { MyDetailsComponent } from './my-details/my-details.component';
import { ChatComponent } from './chat/chat.component';
import { ChatAppComponent } from './chat-app/chat-app.component';
import { ChatService } from '../services/chat.service'
import { PusherService } from '../services/pusher.service'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { faCommentsDollar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core';

library.add( faCommentsDollar);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChatRoutingModule,
    FontAwesomeModule
  ],
  declarations: [MyDetailsComponent, ChatComponent, ChatAppComponent],
  providers: [ChatService, PusherService]
})
export class ChatModule { }

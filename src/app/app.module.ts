import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatModule }     from './chat/chat.module';
import {AngularFireModule} from "angularfire2"
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MytripsdisplayComponent } from './components/mytripsdisplay/mytripsdisplay.component';
import { AddtripsComponent } from './components/addtrips/addtrips.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule, } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { GooglePlacesDirective } from './google-places.directive';
import { RegisterComponent } from './security/register/register.component';
import { LoginComponent } from './security/login/login.component';
import {firebaseConfig} from "../environments/firebase.config";
import { NgxLocalStorageModule } from 'ngx-localstorage';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AuthInterceptor } from './services/auth.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import { DataService } from './services/data.service';
import { TutorialComponent } from './components/tutorial/tutorial.component';
import { GooglePlaces2Directive } from './google-places2.directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core';
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import { faCarAlt } from '@fortawesome/free-solid-svg-icons';
import { faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons';
import { faDonate } from '@fortawesome/free-solid-svg-icons';
import { faHandshake } from '@fortawesome/free-solid-svg-icons';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import { faCalculator } from '@fortawesome/free-solid-svg-icons';
import { faHandHoldingUsd } from '@fortawesome/free-solid-svg-icons';
import { faCommentsDollar } from '@fortawesome/free-solid-svg-icons';
import { ResetpasswordComponent } from './security/resetpassword/resetpassword.component';
import { AlltripsComponent } from './components/alltrips/alltrips.component';
import { AgmCoreModule } from '@agm/core';
import { ngxZendeskWebwidgetModule, ngxZendeskWebwidgetConfig } from 'ngx-zendesk-webwidget';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { FaqComponent } from './components/faq/faq.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AdsenseModule } from 'ng2-adsense';

export class ZendeskConfig extends ngxZendeskWebwidgetConfig {
  accountUrl = 'drivexsg.zendesk.com';
  beforePageLoad(zE) {
    zE.setLocale('en');
    zE.hide();
  }
}

library.add(faMobileAlt, faCarAlt,faMapMarkedAlt,faDonate,faHandshake, faFacebook, faCalculator,faHandHoldingUsd, faCommentsDollar);

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MytripsdisplayComponent,
    AddtripsComponent,
    HomeComponent,
    GooglePlacesDirective,
    RegisterComponent,
    LoginComponent,
    TutorialComponent,
    GooglePlaces2Directive,
    ResetpasswordComponent,
    AlltripsComponent,
    CalculatorComponent,
    FaqComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    ChatModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
   
    AngularFireAuthModule,
     
    NgxLocalStorageModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    FontAwesomeModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBmLt5Mbj0mSiUKpZ6xU_jipZyxg3dJ2X0'
    }),
    ngxZendeskWebwidgetModule.forRoot(ZendeskConfig),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AppRoutingModule,
    AdsenseModule.forRoot({
      adClient: 'ca-pub-3768528920569305',
      adSlot: 7259870550,})
    
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    DataService,
    AuthService, 
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService} from '../../services/auth.service';
import { AuthInfo } from '../../services/auth-info';
import {LocalStorageService} from 'ngx-localstorage';
import { faCarAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isCollapsed = true;
  authInfo: AuthInfo;
  constructor(private authService:AuthService, private _storageService: LocalStorageService) { }
  faCarAlt=faCarAlt
  //userName= null
  ngOnInit() {
    this.authService.authInfo$.subscribe(authInfo => this.authInfo = authInfo);

     //this.userName = this._storageService.get('firebaseEmail')
  }

  logout(){
    this.authService.logout();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytripsdisplayComponent } from './mytripsdisplay.component';

describe('MytripsdisplayComponent', () => {
  let component: MytripsdisplayComponent;
  let fixture: ComponentFixture<MytripsdisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytripsdisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytripsdisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

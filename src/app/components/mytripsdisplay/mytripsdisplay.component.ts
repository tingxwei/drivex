import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TripDetails } from '../../models/tripDetails';
import { GetTripsDetails } from '../../models/getTripsDetails';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormArray, FormControl, FormGroup, Validators, NgForm, Form } from '@angular/forms';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DelKey } from '../../models/deleteKey';

@Component({
  selector: 'app-mytripsdisplay',
  templateUrl: './mytripsdisplay.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .light-blue-backdrop {
      background-color: #db2f63;
    }
    agm-map {
      height: 300px;
    }
  `]
})

export class MytripsdisplayComponent implements OnInit {
  model = {
    pickUpLocation: ''
  }
  lat: number = 1.331460;
  lng: number = 103.849503;
 
  delTrip: Observable<any>
  closeResult: string;
  result: GetTripsDetails[];
  filteredResult: any;
  dotw = ['Mon', 'Tues', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  hour = ['0000', '0100', '0200', '0300', '0400', '0500', '0600', '0700', '0800', '0900', '1000', '1100', '1200', '1300', '1400', '1500', '1600', '1700', '1800', '1900', '2000', '2100', '2200', '2300', '2400']
  mon = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  yr = [2016, 2017, 2018, 2019, 2020, 2021, 2022]
  dy = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
  //filterable properties

  dayOfTheWeek: string;
  month: string;
  n: any;
  m: any;
  e: any;
  year: number;
  pickUpTime: number;
  pickUpTime2: number;
  pickUpLocation: string;
  dropOffLocation: string;
  price: number;
  day: number;

  model2 = null;
  filters = {}

  constructor(private dataService: DataService, private router: Router,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.dataService.getTrips().subscribe(res => {
      this.result = res;
      this.getYear();
      this.filterExact('year', this.year);
     
      
    })
    
  }

  openBackDropCustomClass(content) {
    this.modalService.open(content, { backdropClass: 'light-blue-backdrop' });
  }


  getYear() {
    var d = new Date();
    this.n = d.getFullYear();
    this.year = this.n
    
  }

  private applyFilters() {
    this.filteredResult = _.filter(this.result, _.conforms(this.filters))
  }

  // filter property by day of the week

  filterExact(property: string, rule: any) {
    this.filters[property] = val => val == rule
    this.applyFilters()

  }


  //filter property by time
  filterHourLessThan(property: string, rule: number) {
    this.filters[property] = val => val <= rule
    this.applyFilters()
  }


  filterHourMoreThan(property: string, rule: number) {
    this.filters[property] = val => val >= rule
    this.applyFilters()
  }

  //filter property by greater than or equal price

  filterGreaterThan(property: string, rule: number) {
    this.filters[property] = val => val >= rule
    this.applyFilters()
  }

  removeFilter(property: string) {
    delete this.filters[property]
    this[property] = null
    this.applyFilters()
  }

  filterByLocation(property: string, rule: string) {

    this.filters[property] = val => {
      let val2 = val.toLowerCase();
      console.log(val2)
      let rule2 = rule.toLowerCase();
      return val2.indexOf(rule2) !== -1;
    }
    this.applyFilters()
  }

  checkDeleteTrip(tripKey) {

    this.model2 = new DelKey(tripKey)


  }

  deleteTrip() {
    console.log(this.model2);
    this.dataService.deleteTrip(this.model2).pipe(map(result => result)).subscribe(data => {
      console.log('Trip Deleted');

    })

  }

  ngOnDestroy() {

  }
}
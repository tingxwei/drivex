import { Component, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { faHandshake } from '@fortawesome/free-solid-svg-icons';
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import { faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons';
import { faDonate } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  faHandshake=faHandshake
  faMobileAlt=faMobileAlt
  faMapMarkedAlt=faMapMarkedAlt
  faDonate=faDonate
  constructor(private meta: Meta) {
    this.meta.addTag({ name: 'description', content: 'DriveX SG, the strategic data companion for private hire vehicles (PHV) drivers to maximise their incomes ' });
    this.meta.addTag({ name: 'author', content: 'DriveX SG' });
    this.meta.addTag({ name: 'keywords', content: 'Grab, drive, PHV, private hire, Singapore, income, earnings' });
   }

  ngOnInit() {
  }

}

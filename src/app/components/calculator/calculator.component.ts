import { Component, OnInit } from '@angular/core';
import { faCalculator } from '@fortawesome/free-solid-svg-icons';
import {Calculator} from  '../../models/calculator'
import { Meta } from '@angular/platform-browser';
import { faHandHoldingUsd } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  faHandHoldingUsd=faHandHoldingUsd
  faCalculator=faCalculator
  currentJustify= 'center'
  model= null;
  dailyIncome: number
  hourlyIncome: number
  constructor(private meta: Meta) { this.meta.addTag({ name: 'description', content: 'DriveX SG Income Calculator, a genuine yet simple calculator for your to assess your actual daily income ' });
  this.meta.addTag({ name: 'author', content: 'DriveX SG' });
  this.meta.addTag({ name: 'keywords', content: 'Grab, Ryde, TADA, drive, PHV, private hire, Singapore, income, earnings, calculator' });}

  ngOnInit() {
    this.model = new Calculator(0,0,0,0,0);
    this.Calculator();
    }

    Calculator() {
      this.dailyIncome = this.model.fare - this.model.petrol - this.model.rent - this.model.misc ;
      this.hourlyIncome = this.dailyIncome/this.model.hours
    }
  }






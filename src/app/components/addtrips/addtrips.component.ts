import { Component, OnInit, OnDestroy, NgZone,ViewEncapsulation } from '@angular/core';
import { TripDetails } from '../../models/tripDetails';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormArray, FormControl, FormGroup, Validators, NgForm, Form } from '@angular/forms';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgAnalyzedFile } from '@angular/compiler';
import { GeocodeService } from '../../services/geocode.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-addtrips',
  templateUrl: './addtrips.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
  .light-blue-backdrop {
    background-color: #db2f63;
  }
  agm-map {
    height: 300px;
  }
`]
})
export class AddtripsComponent implements OnInit {

  constructor(private dataService: DataService, private zone: NgZone,  private geocodeService: GeocodeService, private modalService: NgbModal) { }

  tripObv: Observable <any>;
  model = null;
  m: string;
  n: number;
  e: number;
  

  public title = 'addtrip';
  public addrKeys: string[];
  public addrKeys2: string[];
  public addr: object;
  public addr2: object;
  public fAddr = [];
  public fAddr2 = [];
  //Method to be invoked everytime we receive a new instance 
  //of the address object from the onSelect event emitter.
  setAddress(addrObj) {
    //We are wrapping this in a NgZone to reflect the changes
    //to the object in the DOM.
    this.zone.run(() => {
      this.addr = addrObj;
      this.addrKeys = Object.keys(addrObj);
      this.fAddr = Object.values(addrObj);
      this.model.pickUpLocation = this.fAddr[0];
    });
  }

  setAddress2(addrObj2) {
    //We are wrapping this in a NgZone to reflect the changes
    //to the object in the DOM.
    this.zone.run(() => {
      this.addr2 = addrObj2;
      this.addrKeys2 = Object.keys(addrObj2);
      this.fAddr2 = Object.values(addrObj2);
      this.model.dropOffLocation = this.fAddr2[0];
    });
  }

  statuses = ['Completed', 'Cancelled'];
  daysOfTheWeek = ['Mon', 'Tues', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  ngOnInit() {
    this.getMonth();
    this.getDay()
    this.getYear();
 
    this.model = new TripDetails(this.e, this.m, this.n, '', 0, '', 0, '', 0, '', '', 0, 0);
  }

  onSubmit() {

    this.geocodeService.geocodeAddress(this.model.pickUpLocation)
    .subscribe(
      location => {
        console.log(location.lat)
        console.log(location.lng)
        this.model.lat = location.lat
        this.model.lng = location.lng
        this.dataService.addTrip(this.model).pipe(map(result => result)).subscribe(data => {
          console.log('Added');
            this.model = new TripDetails(this.e, this.m, this.n, '', 0, '', 0, '', 0, '', '', 0, 0);
          })
      
      })
 
  }

  openBackDropCustomClass(content) {
    this.modalService.open(content, { backdropClass: 'light-blue-backdrop' });
  }

sendTrip() {

  this.dataService.addTrip(this.model).pipe(map(result => result)).subscribe(data => {
    console.log('Added');
      this.model = new TripDetails(this.e, this.m, this.n, '', 0, '', 0, '', 0, '', '', 0, 0);
    })

}

addressToCoordinates() {

  this.geocodeService.geocodeAddress(this.model.pickUpLocation)
  .subscribe(
    location => {

      console.log(location)
      
   
    }      
  );     
}



  getMonth() {
    var a = new Date();
    const mo = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    this.m = mo[a.getMonth()]
  }

  getYear() {
    var b = new Date();
    this.n = b.getFullYear();
  }

  getDay() {
    var c = new Date();
    this.e = c.getDate();
  }

  ngOnDestroy() {

  }
}

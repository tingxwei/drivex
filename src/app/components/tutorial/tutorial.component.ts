import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Status } from '../../models/status';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css']
})
export class TutorialComponent implements OnInit {

  model = null;
  setPaid: Observable<Status>;
  constructor(private router: Router, private dataService: DataService, ) { }

  ngOnInit() {
    this.model = new Status(1);



  }
  setStatus() {
    this.setPaid = this.dataService.paidStatus(this.model).pipe(map(result => result));
    this.setPaid.subscribe(status => {
      console.log('User has free access', this.model);
      this.router.navigate(['mytrips'])
    });
    
  }

}

export class Calculator {
    constructor(
       public fare: number,
       public rent: number,
       public petrol: number,
       public misc: number,
       public hours: number
     
    ) { }
}

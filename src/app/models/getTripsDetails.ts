export class GetTripsDetails {
    constructor(
       public day: number,
       public month: string,
       public year: number,
       public dayOfTheWeek: string,
       public pickUpTime: number,
       public pickUpTime2: number,
       public pickUpLocation: string,
       public dropOffTime: number,
       public dropOffLocation: string,
       public price: number,
       public status: string,
       public remarks: string,
       public email: string,
       public key: string,
       public lat: number,
       public lng: number,
    ) { }
}

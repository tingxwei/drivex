export class AuthInfo {

    constructor(
        public $uid:string,
        public email:string,
        public emailVerified:boolean
    ) {

    }

    isLoggedIn() {
        return !!this.emailVerified; //null or undefined false  // with value return true
    }


    
}
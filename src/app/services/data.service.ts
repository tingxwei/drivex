import { Injectable } from '@angular/core';
import { AddtripsComponent } from '../components/addtrips/addtrips.component';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError as observableThrowError } from 'rxjs'
import { TripDetails } from '../models/tripDetails';
import { GetTripsDetails } from '../models/getTripsDetails';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { LocalStorageService } from 'ngx-localstorage';
import { Status } from '../models/status';
import { DelKey } from '../models/deleteKey';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // `${environment.backEndApiUrl}`
  private addTripUrl = '/api/addtrip';
  private getTripUrl = '/api/getTrips';
  private saveStatusUrl ='savestatus';
  private deleteTripUrl = '/api/deleteTrip';
  private searchTripUrl = '/api/search';
  constructor(private http: HttpClient, private _storageService: LocalStorageService) { }


  addTrip(trip: TripDetails): Observable<any> {
    console.log(trip);
    let idToken = this._storageService.get('firebaseIdToken');
    return this.http.post<TripDetails>(this.addTripUrl, trip, { headers: new HttpHeaders().set('Authorization', `Bearer ${idToken}`) })
      .pipe(catchError(this.errorHandler));
  }

  getTrips(): Observable<any> {
    console.log('Get MY TRIPS');
    let idToken = this._storageService.get('firebaseIdToken');
    return this.http.get<any>(this.getTripUrl, { headers: new HttpHeaders().set('Authorization', `Bearer ${idToken}`) })
      .pipe(catchError(this.errorHandler))
  }

  paidStatus(stat: Status): Observable<any> {
    let idToken = this._storageService.get('firebaseIdToken');
    return this.http.post<any>(this.saveStatusUrl, stat, { headers: new HttpHeaders().set('Authorization', `Bearer ${idToken}`) })
      .pipe(catchError(this.errorHandler))
  }

  deleteTrip(key: DelKey): Observable<any> {
    let idToken = this._storageService.get('firebaseIdToken');
    console.log(key)
    return this.http.post<any>(this.deleteTripUrl, key, { headers: new HttpHeaders().set('Authorization', `Bearer ${idToken}`) })
    .pipe(catchError(this.errorHandler))
    
  }

  searchByDay(model2): Observable<any> {
    console.log('Get MY TRIPS');
    console.log(model2.dayOfTheWeek)
    let idToken = this._storageService.get('firebaseIdToken');
var searchUrl = `${this.searchTripUrl}?day=${model2.dayOfTheWeek}`
    return this.http.get<any>(searchUrl, { headers: new HttpHeaders().set('Authorization', `Bearer ${idToken}`) })
      .pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Server')
  }
}

import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { Observable, Subject, BehaviorSubject,  throwError as observableThrowError} from "rxjs";
import { catchError } from 'rxjs/operators';
import {AngularFireAuth } from "angularfire2/auth";
import {AuthInfo} from "./auth-info";
import {Router} from "@angular/router";
import {LocalStorageService} from 'ngx-localstorage';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  static UNKNOWN_USER = new AuthInfo(null, null, null);
  public searchResultSource = new Subject<AuthInfo>();//
  authInfo$: BehaviorSubject<AuthInfo> = new BehaviorSubject<AuthInfo>(AuthService.UNKNOWN_USER);
  authState: any = null;
  constructor(private afAuth: AngularFireAuth, private router:Router,
      private _storageService: LocalStorageService, private http: HttpClient) {
          this.afAuth.authState.subscribe((auth) => {
              this.authState = auth
          });
  }

  login(email, password):Observable<AuthInfo> {
      return this.fromFirebaseAuthPromise(this.afAuth.auth.signInWithEmailAndPassword(email, password))
      .pipe(
          catchError(this.handleError('login', AuthInfo))
      );
  }

  

  googleLogin() {
      return new Promise<any>((resolve, reject) => {
          let provider = new firebase.auth.GoogleAuthProvider();
          provider.addScope('profile');
          provider.addScope('email');
          this.afAuth.auth
          .signInWithPopup(provider)
          .then(res => {
              //this.updateUserData()
              resolve(res);
          }, err => {
            console.log(err);
            reject(err);
          })
        })

      //var provider = new firebase.auth.GoogleAuthProvider();
      //return this.socialSignIn(provider);
  }



  signUp(email, password) {
      return this.fromFirebaseAuthPromise2(this.afAuth.auth.createUserWithEmailAndPassword(email, password))
  }

  setTokenIdToLocalstorage(){
      this.afAuth.auth.currentUser.getIdToken().then(idToken => {
          this._storageService.set('firebaseIdToken', idToken);
      });
  }

  fromFirebaseAuthPromise(promise):Observable<any> {
      const subject = new Subject<any>();
      promise
          .then(res => {
                  const authInfo = new AuthInfo(
                          this.afAuth.auth.currentUser.uid, 
                          this.afAuth.auth.currentUser.email,
                        this.afAuth.auth.currentUser.emailVerified,);
                  this.authInfo$.next(authInfo);
                  subject.next(res);
                  subject.complete();
                 
              },
              err => {
                  this.authInfo$.error(err);
                  console.log("err1 " + err);
                  subject.error(err);
                  subject.complete();
              })
      return subject.asObservable();
  }



     fromFirebaseAuthPromise2(promise):Observable<any> {
        const subject = new Subject<any>();
        promise
            .then(res => {
                    const authInfo = new AuthInfo(
                            this.afAuth.auth.currentUser.uid, 
                            this.afAuth.auth.currentUser.email,
                        this.afAuth.auth.currentUser.emailVerified);
                    this.authInfo$.next(authInfo);
                    subject.next(res);
                    subject.complete();
                    let user:any = this.afAuth.auth.currentUser;
                    user.sendEmailVerification().then(
                      (success) => {console.log("please verify your email")} 
                    );
                    //this.updateUserData();
                                 
                },
                err => {
                    this.authInfo$.error(err);
                    console.log("err1 " + err);
                    subject.error(err);
                    subject.complete();
                })
        return subject.asObservable();
    }
  












  logout() {
      this._storageService.remove('firebaseIdToken');
      this.afAuth.auth.signOut();
      this.authInfo$.next(AuthService.UNKNOWN_USER);
      this.router.navigate(['']);

  }

  private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
       
        return observableThrowError(error  || 'backend server error');
      };
    }
  
  
    resetPassword(email: string) {
    
    
        return this.afAuth.auth.sendPasswordResetEmail(email)
          .then(() => console.log("email sent"))
          .catch((error) => console.log(error))
      }
}
